# firerain-welcome
> firerain welcome

[![pipeline status](https://gitlab.com/firerainos/firerain-welcome/badges/master/pipeline.svg)](https://gitlab.com/firerainos/firerain-welcome/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/firerainos/firerain-welcome)](https://goreportcard.com/report/gitlab.com/firerainos/firerain-welcome)
[![GoDoc](https://godoc.org/gitlab.com/firerainos/firerain-welcome?status.svg)](https://godoc.org/gitlab.com/firerainos/firerain-welcome)
[![Sourcegraph](https://sourcegraph.com/gitlab.com/firerainos/firerain-welcome/-/badge.svg)](https://sourcegraph.com/gitlab.com/firerainos/firerain-welcome)

## Build

```bash
git clone https://gitlab.com/firerainos/firerain-welcome
cd firerain-welcome
go build
```

## License
This package is released under [GPLv3](LICENSE).