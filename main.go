package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/theme"
	"gitlab.com/firerainos/firerain-welcome/ui"
)

func main() {
	a := app.New()

	a.Settings().SetTheme(theme.LightTheme())

	mainWindow := ui.NewMainWindow(a)
	mainWindow.SetFixedSize(true)
	mainWindow.Resize(fyne.NewSize(800,600))
	mainWindow.CenterOnScreen()

	mainWindow.ShowAndRun()
}
