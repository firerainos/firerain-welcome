package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"gitlab.com/firerainos/firerain-welcome/resources"
	widget2 "gitlab.com/firerainos/firerain-welcome/ui/widget"
	"net/url"
)

type MainWindow struct {
	fyne.Window
}

func NewMainWindow(app fyne.App) *MainWindow {
	w := &MainWindow{app.NewWindow("FireRain Welcome")}

	logoIcon := widget2.NewIconWithSize(resources.RescourceLogoPng, fyne.NewSize(150, 150))

	infoLabel := widget.NewLabel("FireRain OS is a user-friendly Linux distribution based on Arch Linux that provides graphical operations and guarantees the nativeness of Arch.")
	infoLabel.Wrapping = fyne.TextWrapWord

	gridContainer := container.NewGridWithColumns(3)

	gridContainer.Add(w.newCenterLabel("Documentation"))
	gridContainer.Add(w.newCenterLabel("Support"))
	gridContainer.Add(w.newCenterLabel("Project"))

	gridContainer.Add(w.newHyperlink("Arch Linux Wiki", "https://wiki.archlinux.org"))
	gridContainer.Add(w.newHyperlink("Telegram Group", "https://t.me/FireRainOS"))
	gridContainer.Add(w.newHyperlink("Gitlab", "https://gitlab.com/firerainos"))

	c := container.NewVBox(
		layout.NewSpacer(),
		container.NewCenter(logoIcon),
		container.NewCenter(widget.NewLabel("Welcome to FireRain OS!")),
		infoLabel,
		layout.NewSpacer(),
		container.NewCenter(gridContainer),
		layout.NewSpacer(),
		container.NewCenter(widget.NewButton("Exit", app.Quit)),
	)

	w.SetContent(c)

	return w
}

func (w *MainWindow) newCenterLabel(text string) *widget.Label {
	label := widget.NewLabel(text)
	label.Alignment = fyne.TextAlignCenter
	return label
}

func (w *MainWindow) newHyperlink(text, urlStr string) *widget.Hyperlink {
	u, _ := url.Parse(urlStr)
	link := widget.NewHyperlink(text, u)
	link.Alignment = fyne.TextAlignCenter
	return link
}
