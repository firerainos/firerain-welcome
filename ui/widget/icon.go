package widget

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

type Icon struct {
	widget.Icon
}

func NewIconWithSize(res fyne.Resource, size fyne.Size) *Icon {
	i := &Icon{}
	i.ExtendBaseWidget(i)
	i.SetResource(res)
	i.Resize(size)
	return i
}

func (i *Icon) MinSize() fyne.Size {
	i.ExtendBaseWidget(i)
	return i.Size()
}
